<?php

namespace app\models;

use yii\base\Model;

class User extends Model
{
    public $id;
    public $external_id;

    public function rules()
    {
        return [
            [['id', 'external_id'], 'integer']
        ];
    }
}
