<?php

namespace app\classes;

use app\models\CoinKeeper;
use app\models\User;

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 18.09.18
 * Time: 20:20
 */
class KeeperManager
{
    public static function createUsdKeeper(User $user)
    {
        if (!CoinKeeper::isKeeperExist($user->id, CoinKeeper::USD_KEEPER)) {
            $model = new CoinKeeper(['uid' => $user->id, 'external_uid' => $user->external_id, 'type' => CoinKeeper::USD_KEEPER]);
            $model->save();
        }
    }

    public static function getUserKeepers($uid, $filter = [])
    {
        return CoinKeeper::find()->Where(['uid' => $uid])->all();
    }
}