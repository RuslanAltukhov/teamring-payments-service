<?php

namespace app\controllers;

use app\classes\KeeperManager;

/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 19.09.18
 * Time: 21:50
 */
class ApiController extends \yii\rest\ActiveController
{
    public $modelClass = "app\\models\\CoinKeeper";

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['contentNegotiator'] = [
            'class' => \yii\filters\ContentNegotiator::className(),
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];
        return $behaviors;
    }

    public function actionGetUserKeepers()
    {
        try {
            $data = KeeperManager::getUserKeepers(\Yii::$app->request->get("uid", []));
            return ['code' => 0, 'data' => $data];
        } catch (\yii\base\Exception $e) {
            return ['code' => 3, 'text' => 'Ошибка сервера. Повторите позже'];
        }
    }
}