<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coin_keepers".
 *
 * @property int $id
 * @property int $uid
 * @property int $external_uid
 * @property string $type
 * @property int $balance
 * @property string $created_at
 */
class CoinKeeper extends \yii\db\ActiveRecord
{
    const USD_KEEPER = "USD";

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coin_keepers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uid', 'external_uid', 'balance'], 'integer'],
            [['type'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    public static function isKeeperExist(int $uid, string $type)
    {
        return static::find()->where(['uid' => $uid, 'type' => $type])->exists();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'external_uid' => 'External Uid',
            'type' => 'Type',
            'balance' => 'Balance',
            'created_at' => 'Created At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return CoinKeepersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CoinKeepersQuery(get_called_class());
    }
}
