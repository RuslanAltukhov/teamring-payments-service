<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CoinKeeper]].
 *
 * @see CoinKeeper
 */
class CoinKeepersQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CoinKeeper[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CoinKeeper|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
