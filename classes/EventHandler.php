<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 31.08.18
 * Time: 16:00
 */

namespace app\classes;


use app\models\data\Message;
use app\models\data\RestoreRequest;
use app\models\User;
use yii\base\Event;

class EventHandler
{
    private static $_instance;

    private function __construct()
    {

    }

    private function __clone()
    {
    }

    public function send(Message $data)
    {
        if ($method = $this->getHandlerByEventName($data->event_name)) {
            $this->{$method}($data);
        }
    }

    public function getHandlerByEventName(string $event_name)
    {
        $method_name = strtolower(preg_replace("/(\_)/i", "", $event_name));
        $methods = get_class_methods(self::class);
        foreach ($methods as $method) {
            if (strtolower($method) == $method_name) {
                return $method;
            }
        }
    }

    public function onUserCreated(Message $data)
    {
        \Yii::$app->getDb()->open();
        $user = new User();
        if ($user->load((array)$data->data, '')) {
            KeeperManager::createUsdKeeper($user);
        } else {
            echo "invalid input data for event";
        }
    }

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

}