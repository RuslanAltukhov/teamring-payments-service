<?php

use yii\db\Migration;

/**
 * Handles the creation of table `coin_keepers`.
 */
class m180918_160526_create_coin_keepers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('coin_keepers', [
            'id' => $this->primaryKey(),
            'uid' => $this->bigInteger(20),
            'external_uid' => $this->bigInteger(20),
            'type' => 'enum("USD","TOKEN")',
            'balance' => $this->bigInteger(20)->defaultValue(0),
            'created_at' => $this->timestamp()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('coin_keepers');
    }
}
