<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 31.08.18
 * Time: 16:29
 */

namespace app\classes;


use app\models\data\Message;
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class EventDispatcher
{
    private $connection;
    private $channel;
    private static $_instance;
    private $exchange = "users";
    private $queue_name = "payments_service_queue";
    private $handler;

    private function __clone()
    {
        // TODO: Implement __clone() method.
    }

    public static function getInstance()
    {
        if (!self::$_instance)
            self::$_instance = new self();
        return self::$_instance;
    }

    private function __construct()
    {
        $this->handler = EventHandler::getInstance();
        $params = \Yii::$app->params['rabbitmq'];
        $this->connection = new AMQPConnection($params['host'],
            $params['port'],
            $params['username'],
            $params['password']);
        $this->channel = $this->connection->channel();

        $this->channel->exchange_declare($this->exchange,
            'fanout',
            false,
            true,
            false);
        $this->channel->queue_declare($this->queue_name, false, true, false,false);
         $this->channel->queue_bind($this->queue_name, $this->exchange);
        $this->channel->basic_consume($this->queue_name,
            '', false,
            true,
            false,
            false,
            function ($msg) {
                $this->onMessage($msg);
            });
        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }

    }

    private function onMessage(AMQPMessage $msg)
    {
        $model = new Message(json_decode($msg->body));
        $this->handler->send($model);
    }

    public function __destruct()
    {
        $this->channel->close();
        $this->connection->close();
    }
}